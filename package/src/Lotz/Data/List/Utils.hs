{- |
   Module     : Lotz.Data.List.Utils
   Copyright  : Copyright (C) 2011 Manfred Lotz 
   License    : GNU LGPL, version 2.1 or above

   Maintainer : Manfred Lotz <manfred.lotz@yahoo.de>
   Stability  : provisional
   Portability: portable

Additional list functions not to be found in the standard.

Copyright (c) 2011 Manfred Lotz, manfred.lotz\@arcor.de
-}
module Lotz.Data.List.Utils
       (
         splitWith, replaceAll
       ) where





{- | Split a list into a list of lists using a function a -> Bool.

Example:

>  splitWith isSpace "this is a sentence"

-}
       
splitWith :: (a -> Bool) -> [a] -> [[a]]
splitWith _ [] = []
splitWith p xs =
  let (pre,suf) = break p xs
  in pre : case suf of
    (_:xs')  -> splitWith p xs'
    [] -> []



{- | Replace all occurences of find in s by repl. 

Example:
> let find = "needle" in
> let repl = "hammer" in
> replaceAll find repl "a needle in a haystack full of needles"
   
-}

replaceAll :: Eq a => [a] -> [a] -> [a] -> [a]
replaceAll _ _ [] = []
replaceAll find repl s =
    if take (length find) s == find
        then repl ++ replaceAll find repl (drop (length find) s)
        else [head s] ++ replaceAll find repl (tail s) 
