{- |
   Module     : Lotz.Data.Time.Utils
   Copyright  : Copyright (C) 2011 Manfred Lotz 
   License    : GNU LGPL, version 2.1 or above

   Maintainer : Manfred Lotz <manfred.lotz@yahoo.de>
   Stability  : provisional
   Portability: portable

Additional time functions not to be found in the standard.

Copyright (c) 2011 Manfred Lotz, manfred.lotz\@arcor.de
-}
module Lotz.Data.Time.Utils
       (
         localtime, LTime (..) 
       ) where

import Data.List
import Data.Time



data LTime = LTime {
   ltYear :: String
  ,ltMonth :: String
  ,ltDay :: String
  ,ltHour :: String
  ,ltMin :: String
  ,ltSec :: String
  ,ltTz :: String
  } deriving (Show,Read)

             
-- perl localtime returns:
--   Sat Jan  7 18:13:41 2012
-- getZonedTime returns:
-- 2012-01-07 18:14:06.09973 CET

localtime :: IO LTime
localtime = do zt <- getZonedTime
               let [date,time,tzone] = split " " (show zt)
               let [y,mon,d] = split "-" date
               let [h,m,s'] = split ":" time
               let [s,_] = split "." s'
               return LTime { ltYear = y
                            , ltMonth = mon
                            , ltDay = d
                            , ltHour = h
                            , ltMin = m
                            , ltSec = s
                            , ltTz = tzone
                            } 


--
-- The following is from MissingH Data.List.Utlis.
-- I inserted it here because I didn't want the dependency.
--

{-
  Example:
            
   split "," "foo,bar,,baz," -> ["foo", "bar", "", "baz", ""]

   split "ba" ",foo,bar,,baz," -> [",foo,","r,,","z,"]
-}
split :: Eq a => [a] -> [a] -> [[a]]
split _ [] = []
split delim str =
    let (firstline, remainder) = breakList (isPrefixOf delim) str
        in 
        firstline : case remainder of
                                   [] -> []
                                   x -> if x == delim
                                        then [] : []
                                        else split delim 
                                                 (drop (length delim) x)

breakList :: ([a] -> Bool) -> [a] -> ([a], [a])
breakList func = spanList (not . func)



{- | Similar to Data.List.span, but performs the test on the entire remaining
list instead of just one element. 

@spanList p xs@ is the same as @(takeWhileList p xs, dropWhileList p xs)@ 
-}
spanList :: ([a] -> Bool) -> [a] -> ([a], [a])

spanList _ [] = ([],[])
spanList func list@(x:xs) =
    if func list
       then (x:ys,zs)
       else ([],list)
    where (ys,zs) = spanList func xs