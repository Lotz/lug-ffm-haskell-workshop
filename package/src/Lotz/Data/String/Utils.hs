{- |
   Module     : Lotz.Data.String.Utils
   Copyright  : Copyright (C) 2011 Manfred Lotz 
   License    : GNU LGPL, version 2.1 or above

   Maintainer : Manfred Lotz <manfred.lotz@yahoo.de>
   Stability  : provisional
   Portability: portable

Additional string functions not to be found in the standard.

Copyright (c) 2011 Manfred Lotz, manfred.lotz\@arcor.de
-}
module Lotz.Data.String.Utils
       (
         trimBeg, trimEnd, trim
       ) where

import Data.Char


trimBeg, trimEnd, trim :: String -> String
trimBeg = snd . span isSpace
trimEnd = reverse . trimBeg . reverse
trim = trimEnd . trimBeg



       