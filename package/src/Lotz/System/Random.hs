module Lotz.System.Random
       (rand,
        randI
       ) where


import System.Random


{- | Returns a random number between 0 and m. 
-}
rand  :: Int -> IO Int
rand m = getStdRandom (randomR (0,m))

         
{- | Returns a random number between m and n. 
-}
randI  :: Int -> Int -> IO Int
randI m n = getStdRandom (randomR (m,n))