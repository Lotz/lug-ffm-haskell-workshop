module Lotz.System.Shell
       (osCmd,
        osShell
       ) where

import System.IO
import System.Exit
import System.IO.Error (isEOFError)
import System.Posix.IO
import System.Process
import Control.Monad
import Control.Exception.Base
import Prelude hiding (catch)
import Data.List



{- | Issue command. You must provide a function for displaying the output of
     the command. Such a function could be for instance: putStrLn

     osCmd returns the outcome of the command. 

-}
osCmd :: (String -> IO ()) -> String -> [String] -> IO Int
osCmd logfun cmd parms = do
    let p' = intercalate " " parms
    logfun $ "Issuing: " ++ cmd ++ " " ++ p'

    (inFd,outFd) <- createPipe
    inHdl <- fdToHandle inFd
    outHdl <- fdToHandle outFd
    (_ ,_ ,_ ,cmdproc) <- createProcess (proc cmd parms) {
      std_out = UseHandle outHdl,
      std_err = UseHandle outHdl 
    }
    printCmd logfun inHdl `catch` exhandler `finally` hClose inHdl
    rc <- waitForProcess cmdproc
    return $ 
            case rc of
               ExitSuccess -> 0
               ExitFailure r -> r
  where
    exhandler :: IOException -> IO ()
    exhandler e
        | isEOFError e = return ()
    exhandler e = print e




{- | Issue a shell command. You must provide a function for displaying the output of
     the command. Such a function could be for instance: putStrLn

     osShell returns the outcome of the command. 

-}
osShell :: (String -> IO ()) -> String -> IO Int
osShell logfun cmd = do
    logfun $ "Issuing: " ++ cmd

    (inFd,outFd) <- createPipe
    inHdl <- fdToHandle inFd
    outHdl <- fdToHandle outFd
    (_ ,_ ,_ ,cmdproc) <- createProcess (shell cmd ) {
      std_out = UseHandle outHdl,
      std_err = UseHandle outHdl 
    }
    printCmd logfun inHdl `catch` exhandler `finally` hClose inHdl
    rc <- waitForProcess cmdproc
    return $ 
            case rc of
               ExitSuccess -> 0
               ExitFailure r -> r
  where
    exhandler :: IOException -> IO ()
    exhandler e
        | isEOFError e = return ()
    exhandler e = print e




printCmd :: (String -> IO ()) -> Handle -> IO () 
printCmd logfun inHdl = do
  s <- hGetLine inHdl
  logfun s
  printCmd logfun inHdl