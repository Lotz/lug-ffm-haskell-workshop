module Main where

import System.IO

main :: IO ()
main = do
  putStrLn "Password will be echoed using asteriscs"
  pwd <- askPassword 
  putStrLn $ "\nPassword typed was: " ++ pwd


getPassword :: IO String
getPassword = do c <- getChar
                 if c == '\n'
                   then return ""
                   else do putChar '*'
                           l <- getPassword
                           return (c:l)


askPassword :: IO String
askPassword = do hSetEcho stdin False
                 hSetBuffering stdout NoBuffering
                 putStr "Enter password please: "
                 getPassword
              
                  
              