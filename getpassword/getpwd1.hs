module Main where

--
-- ghci works the same way as the executable
--
main :: IO ()
main = do
  putStrLn "Password will be shown which is bad"
  pwd <- askPassword 
  putStrLn $ "\nPassword typed was: " ++ pwd

getPassword :: IO String
getPassword = do c <- getChar
                 if c == '\n'
                   then return ""
                   else do l <- getPassword
                           return (c:l)

askPassword :: IO String
askPassword = do putStr "Enter password please: "
                 getPassword
              