module Main where

import System.IO

main :: IO ()
main = do
  putStrLn "Password will not be echoed" 
  pwd <- askPassword 
  putStrLn $ "\nPassword typed was: " ++ pwd

getPassword :: IO String
getPassword = do c <- getChar
                 if c == '\n'
                   then return ""
                   else do l <- getPassword
                           return (c:l)

askPassword :: IO String
askPassword = do hSetEcho stdin False
                 putStr "Enter password please: "
                 getPassword
              